
# <img src="man/figures/logo_optirrigMANUAL_200px.png" alt="Optirrig" align="right" style="width:200px"/> optirrigMANUAL: User Manual & Documentation : R Package support

<!-- badges: start -->

[![CRAN](https://www.r-pkg.org/badges/version-ago/optirrigMANUAL)](https://forgemia.inra.fr/OptirrigHIVE/optirrigMANUAL.git)
[![License:
GPL-3](https://img.shields.io/badge/license-GPL--3-orange.svg)](https://cran.r-project.org/web/licenses/GPL-3)
[![Dev check
status](https://forgemia.inra.fr/OptirrigHIVE/optirrigMANUAL/badges/main/pipeline.svg)](forgemia.inra.fr/OptirrigHIVE/optirrigMANUAL/-/pipelines)
<!-- badges: end -->

Optirrig is a conceptual 1D model with multiple functionalities. Its
purpose is to simulate and optimize irrigation scenarios based on the
type of crop. It incorporates climatic forcings, and accounts for
biological processes (plant development, leaf surface index, dry matter
rates, water and nitrogen stress).

The purpose of this package is to serve as a basis for developing and
integrating functionality as required.

## Installation

You can install the development version of optirrigMANUAL like so:

``` r
install.packages("remotes")
remotes::install_git("https://read_only:R77TN1F7k-n7PqoixcRh@forgemia.inra.fr/OptirrigHIVE/optirrigMANUAL.git", build_vignettes = TRUE)
```

## Launch the library

``` r
library(optirrigCORE)
library(optirrigMANUAL)
```

## Documentation

UMR G-EAU - Gestion de l’Eau, Acteurs, Usages (Montpellier)

<https://www.g-eau.fr/index.php/fr/>

Equipe OPTIMISTE - Optimisation du Pilotage et des Technologies
d’Irrigation, Minimisation des IntrantS, Transferts dans l’Environnement

<https://www.g-eau.fr/index.php/fr/umr-geau/les-equipes/item/410-optimisation-du-pilotage-et-des-technologies-d-irrigation-minimisation-des-intrants-transferts-environnementaux-optimiste>

<div style="display: flex; justify-content: space-between; align-items: center;">

<img src="man/figures/Logo-INRAE_Transparent.svg.png" alt="G-EAU" width="300"/><img src="man/figures/logo_G-EAU.jpg" alt="G-EAU" width="200"/><img src="man/figures/log_Optirrig.png" alt="G-EAU" width="200"/>
